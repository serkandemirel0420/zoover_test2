var fs = require('fs');
var _ = require('lodash');
var moment = require("moment");

function readData(path) {
    return new Promise(function(resolve, reject) {
        fs.readFile(path,"utf8", function(err, data){
            if (err)
                reject(err);
            else
                resolve(data);
        });
    });
}


function calculateWeightRatings(data) {
    return _.map(data, function(obj) {

        var clone = _.cloneDeep(obj);
        //TODO add weight calculations here as single line
        var calc_ratings = {}
        for (var i in clone.ratings) {
            calc_ratings[i] = {}
            for (var j in clone.ratings[i]) {
                calc_ratings[i][j] = clone.ratings[i][j] * clone.weight
            }
        }

        clone.calculate_ratings = calc_ratings;
        // TODO add total count and avg calculations here.
        return clone;
    });
}

function calculateWeight(data) {
    return data.map(function(arr){

        var ageDifMs = Date.now() -  arr.entryDate;
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        let difference =  Math.abs(ageDate.getUTCFullYear() - 1970);

        var weight = null;

        if(difference > 5){
            weight = 0.5;
        }else{
            weight =  (1 - (difference)*0.1)
        }
        arr['weight'] = weight;
        return arr;
    });
}



var avg = (data) => _.mergeWith({}, ...data, (a, b, c) => {
    if(_.isNumber(b)) {
        return ((b || 0) / data.length) + (_.isNumber(a) ? (a || 0) : 0);
    }
});

module.exports = {
    readData : readData,
    avg : avg,
    calculateWeight : calculateWeight,
    calculateWeightRatings: calculateWeightRatings
}