var express = require('express');
var router = express.Router();
//utulity lib for reading data
var dataUtil = require('./../lib/data');
var path = require('path');
var _ = require("lodash");


/* GET users listing. */
router.get('/', async function(req, res, next) {

    debugger;
    //relative path of the data file
    let relPath = '../data/review.json';
    //full path resolve
    let fullPath = path.resolve(__dirname, relPath);

    try{
        var data = await dataUtil.readData(fullPath);
        data = JSON.parse(data);
        // var filteredData = JSON.parse(data).map(function(arr){
        //     return arr.ratings
        // });

        var weightedData = dataUtil.calculateWeight(data);
        var weightedRatingData = dataUtil.calculateWeightRatings(weightedData);
        var  uniqueTravelWidth = _.uniq(_.map(weightedRatingData, 'traveledWith')).map(function(e){ return { key: e, value:e };})


        var offset = Number(req.query.offset);
        var traveledWith = req.query.traveledWith;
        var orderBy = req.query.orderBy;
        var orderType = req.query.orderType;


        if(isNaN(Number(offset)))
             offset = 0;

        if(traveledWith)
            weightedRatingData =_.filter(weightedRatingData, {traveledWith: traveledWith});

        //TODO problem with sortby
        if(orderBy == "travelDate" || orderBy == "entryDate"  ){
            if(orderType != "asc" && orderType != "desc" ){
                orderType=null;
            }
            weightedRatingData = _.orderBy(weightedRatingData, [orderBy], [(orderType || "asc")]);
        }


        var tenWeightedRatingData = _.take(_.drop(weightedRatingData, offset), 10);

        var filteredData = weightedRatingData.map(function(arr){
            return arr.calculate_ratings;
        });

        var avg = dataUtil.avg(filteredData);

        //filteredData.slice(offset, offset + per_page);


    }catch (err){
        //handle the error through the middleware  - TODO: impl. logging
        return next(err);
    }
    var length = filteredData.length;

    var result = _.merge({"weightedRatingData" :tenWeightedRatingData,"avg": avg, length : length, uniqueTravelWidth: uniqueTravelWidth});
    return res.send(result);

});

module.exports = router;
