import React, {Component} from 'react';
import Items from './Items/Items';

import './App.css';

class App extends Component {

    handleClick(){
        alert("Hellowww")
    }

    render() {
        return (
            <div className="App">
                <div>This is app component!</div>
                <button onClick={this.handleClick}>Helloww</button>
                <Items/>
            </div>
        );
    }
}

export default App;
