import React, { Component } from 'react';
import './Items.css';
import Average from '../Average/Average'
import ReactPaginate from 'react-paginate';
import  Filter from '../Filter/Filter'
import  Item from '../Item/Item'
import Order from '../Order/Order'

import axios from 'axios';

class ItemsComponent extends Component {


    loadData(offset = 0,reset){

        debugger;

        var _this = this;

        var traveledWith = document.getElementById("traveledWith");
         traveledWith = (traveledWith ? "&traveledWith=" +  document.getElementById("traveledWith").value : "");
        var orderBy = document.getElementById("orderBy");
         orderBy = (orderBy ? document.getElementById("orderBy").value : "");

            axios
                .get('http://localhost:3001/review?offset='+ offset + traveledWith + orderBy)
                .then((result) => {
                    var reviews = result.data;
                    console.log("request made");
                    _this.setState({
                        reviews
                    });
                    window.scrollTo(0, 0)
                });

    }

    componentWillMount(){

    }

    componentDidMount(){
        this.loadData()
    }


    filterByTraveledWidth(){

        this.setState({
            current:0
        })

        this.loadData(null);

    }

    length(){
        return this.state.reviews.weightedRatingData.length
    }
// TODO seperate to three components  pass data via props

    handlePageClick(e){

        console.log("handle click worked")

        this.setState({
            current:e.selected
        });

        let count = (e.selected ) * 10;
        this.loadData(count);
    }

    orderByClick(e){
        this.loadData()
        this.setState({
            current:0
        });
    }


    render() {
        if (!this.state) {
            return <div>Loading...</div>
        }

        return (
            <div>
                <h2>Accomodation</h2>

                <Average average={this.state.reviews.avg}/>

                <Filter items={this.state.reviews.uniqueTravelWidth}  filter={this.filterByTraveledWidth.bind(this)}/>

                <Order id={"orderBy"}
                       change={this.orderByClick.bind(this)}
                       text={"Order By Review Date"}/>




                {this.state.reviews.weightedRatingData.map((item,i) =>
                    <Item key={item.id} values={item}/>
                )}


                <ReactPaginate previousLabel={"previous"}
                               nextLabel={"next"}
                               breakLabel={<a >...</a>}
                               breakClassName={"break-me"}
                               pageCount={(this.state.reviews.length / 10)}
                               marginPagesDisplayed={2}
                               pageRangeDisplayed={4}
                               onPageChange={this.handlePageClick.bind(this)}
                               containerClassName={"pagination"}
                               subContainerClassName={"pages pagination"}
                               activeClassName={"active"}
                               forcePage={this.state.current}/>
            </div>
        );
    }
}

export default ItemsComponent;
