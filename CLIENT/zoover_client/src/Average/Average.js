import React, { Component } from 'react';
import './Average.css';



class AverageComponent extends Component {

    componentWillMount(){

    }

    length(){
        return this.props.average.length
    }

    render() {


        return (
            <div>

                <div className="general">
                    <h2 className="general"> {this.length()} Review</h2>
                    <div className="general">{this.props.average.general.general}</div>
                    <div className="location">{this.props.average.aspects.location}</div>
                    <div className="priceQuality">{this.props.average.aspects.priceQuality}</div>
                    <div className="restaurants">{this.props.average.aspects.restaurants}</div>
                </div>
            </div>
        );
    }
}

export default AverageComponent;
