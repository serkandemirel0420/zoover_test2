/**
 * Created by serkandemirel on 14/05/2017.
 */
import React, { Component } from 'react';

class Item extends Component {

    dateConvert(date){

        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        var d = new Date(date);
        return monthNames[d.getMonth()] + " " +  d.getFullYear();
        // TODO convert to string literal
    }

    render(){
        return (
            <div className="review" key={this.props.values.id}>
                <h1 className="title">  {Object.values(this.props.values.titles)[0] }</h1>
                <div className="ratings">
                    <div className="item general"> General : {this.props.values.ratings.general.general}</div>
                    <div className="item location">Location : {this.props.values.ratings.aspects.location}</div>
                    <div className="item priceQuality">Price Quality : {this.props.values.ratings.aspects.priceQuality}</div>
                    <div className="item restaurants">Restaurants : {this.props.values.ratings.aspects.restaurants}</div>
                </div>
                <div className="text"> : {this.props.values.texts.nl}</div>
                <br/>
                <div className="travelDate">Travelled in { this.dateConvert(this.props.values.travelDate) }</div>
                <div className="travelDate">Revieved in { this.dateConvert(this.props.values.entryDate) }</div>

                <hr />

            </div>
        )


    }

}

export default Item;