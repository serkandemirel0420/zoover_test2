import React, { Component } from 'react';
import './Filter.css';

import { Button } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';

class FilterComponent extends Component {

    passDataToParent(e){
        debugger;
        this.props.filter(null,0)
    }

    render(){
        return(


                <select id="traveledWith"  onChange={this.passDataToParent.bind(this)}>
                    <option value="">Filter</option>
                    {this.props.items.map(function (item) {
                        return <option key={item.key} value={item.value}>{item.value}</option>
                    })}
                </select>


        )
    }
}

export default FilterComponent